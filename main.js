const Apify = global.Apify = require('apify');
// const puppeteer = require(require.resolve('/home/ubuntu/.nvm/versions/node/v10.11.0/lib/node_modules/apify/node_modules/puppeteer') || require.resolve('puppeteer'));
const { shot, error, getSpreadsheet, queueUrls, trunc, gotoFunction, handlePageFunction, isFinishedFunction } = require('apify-matcher-utils')(Apify);

Apify.main(async () => {
  
  const config = await require('./config.js')(Apify);
  
  let INPUT = await Apify.getValue('INPUT');
  const { max, start, limit, country, startUrls, webhookUrl, webhookData } = INPUT;
  
  const requestQueue = global.requestQueue = await Apify.openRequestQueue();
  queueUrls(startUrls || getStartUrls(), requestQueue, limit);
  
  // Crawler
  let crawler = new Apify.PuppeteerCrawler({
    requestQueue,
    ...config.crawler,
    handlePageFunction: async data => {
      return await handlePageFunction(data, { pageMatcherSettings: config.matcher, requestQueue })
    },
    gotoFunction: async data => {
      return await gotoFunction(data, { pageMatcherSettings: config.matcher, requestQueue })
    },
    isFinishedFunction,
    handleFailedRequestFunction: (data) => {
      console.log('FAILED', { data });
    }
  });
  
  await crawler.run();
  
});

function getStartUrls(){
  return [
    {url: 'https://www.asos.com/iceberg/iceberg-twisted-seam-skinny-jeans/prd/9394448?clr=degradable-blue&SearchQuery=&cid=20502&gridcolumn=1&gridrow=1', matcherLabel: 'product', mapped_category: 'jeans' }
    // { url: 'https://www.asos.com/women/sale/jeans/skinny-jeans/cat/?cid=20502', mapped_category: 'jeans' }
    // { url: 'https://www.asos.com/women/sale/jeans/cat/?cid=4331&ctaref=shop%7Cjeans%7Cwomen_hp_sale', mapped_category: 'jeans' }
  ]
}